//
//  TeamsTableViewController.swift
//  NW_Programming_Contest
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class TeamsTableViewController: UITableViewController {

    var schoolSegue:School!

    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolSegue.teams.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentTable", for: indexPath)
        cell.textLabel?.text = schoolSegue.teams[indexPath.row].name
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "studentDetails" {
            let studentValues = segue.destination as! StudentViewController
            studentValues.studentSegue = schoolSegue.teams[tableView.indexPathForSelectedRow!.row]
        }
        else if segue.identifier == "newTeam" {
            let newteamadd = segue.destination as! NewTeamViewController
            newteamadd.addnewTeam = schoolSegue
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            schoolSegue.teams.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    
    override func viewDidLoad() {
        navigationItem.title = schoolSegue.name
        super.viewDidLoad()
    }
    
}
