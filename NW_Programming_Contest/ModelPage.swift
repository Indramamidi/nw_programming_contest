//
//  ModelPage.swift
//  NW_Programming_Contest
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
class School : Equatable
    
{
    var name:String!
    var coach:String!
    var teams:[Team]!
    init(name:String,coach:String)
    {
        self.name = name
        self.coach = coach
        self.teams = []
    }
    func addTeam(name:String,students:[String])
    {
        teams.append(Team(name: name, students: students))
    }
    static func == (lhs: School, rhs: School) -> Bool {
        return lhs.name == rhs.name && lhs.coach == rhs.coach && lhs.teams == rhs.teams
    }
}
class Team:Equatable
{
    var name:String = ""
    var students:[String]!
    static func == (lhs: Team, rhs: Team) -> Bool {
        return lhs.name == rhs.name && lhs.students == rhs.students
    }
    init(name:String,students:[String])
    {
        self.name = name
        self.students = students
    }
}

class Schools
{
    static var shared = Schools()
    private var schools:[School] = []
    init(schools: [School]) {
        self.schools = schools
    }
    private init()
    {
        self.schools = []
    }
    func numSchools()->Int {
        return schools.count
    }
    func school(_ index:Int) -> School {
        return schools[index]
    }
    subscript(index:Int) -> School {
        return schools[index]
    }
    func add(school:School){
    schools.append(school)
    }
    func delete(school:School){
        for i in 0 ..< schools.count {
            if schools[i] == school {
                schools.remove(at:i)
                break
            }
        }
    }
}
