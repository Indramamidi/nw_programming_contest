//
//  StudentViewController.swift
//  NW_Programming_Contest
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class StudentViewController: UIViewController {
    
    
    @IBOutlet weak var student0: UILabel!
    @IBOutlet weak var student2: UILabel!
    @IBOutlet weak var student1: UILabel!
    
    var studentSegue:Team!
    override func viewDidLoad() {
        navigationItem.title = studentSegue.name
        student0.text = studentSegue.students[0]
        student1.text = studentSegue.students[1]
        student2.text = studentSegue.students[2]
        super.viewDidLoad()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
