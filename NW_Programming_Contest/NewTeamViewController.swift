//
//  NewTeamViewController.swift
//  NW_Programming_Contest
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {

    @IBOutlet weak var studentTF2: UITextField!
    @IBOutlet weak var studentTF1: UITextField!
    @IBOutlet weak var studentTF0: UITextField!
    @IBOutlet weak var newTeamTF: UITextField!
    var addnewTeam:School!
    override func viewDidLoad() {
        
        print(addnewTeam.name)
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

     @IBAction func done(_ sender: Any) {
        
        addnewTeam.addTeam(name: newTeamTF.text!, students: [studentTF0.text!,studentTF1.text!,studentTF2.text!])
        self.dismiss(animated: true, completion: nil)
     }
 
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
