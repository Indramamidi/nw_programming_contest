//
//  SchoolsTableTableViewController.swift
//  NW_Programming_Contest
//
//  Created by student on 3/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class SchoolsTableTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Schools.shared.numSchools()
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "schooldetails")
        cell!.textLabel?.text =  Schools.shared[indexPath.row].name
        cell!.detailTextLabel?.text = Schools.shared[indexPath.row].coach
        return cell!
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.identifier == "team" {
        let teams = segue.destination as! TeamsTableViewController
        teams.schoolSegue = Schools.shared[tableView.indexPathForSelectedRow!.row]
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            Schools.shared.delete(school: Schools.shared[indexPath.row])
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
}
